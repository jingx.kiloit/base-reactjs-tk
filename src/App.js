import {BrowserRouter, Route, Routes} from "react-router-dom"
import Login from "./modules/auth/components/Login";
import Layout from "./modules/layout/Layout";
import NotFoundPage from "./modules/error";
import {useSelector} from "react-redux";
import {useEffect} from "react";
import {useAuth} from "./modules/auth/core/hook";
import {routs} from "./modules/constants/routes";
import {RoutHasPermission} from "./modules/components/HasPermission";

function App() {
    const {token} = useSelector(state => state.auth);
    const {onGetProfile} = useAuth();


    useEffect(() => {
        token && onGetProfile();
        // eslint-disable-next-line
    }, [token]);

    return (
        <BrowserRouter>
            <Routes>
                {token ? <Route path="/" element={<Layout/>}>
                    {routs.map((r, index) => {
                        return <Route key={index} path={r.path} index={r.exact} element={
                            <RoutHasPermission permission={r.permission}>
                                <r.component/>
                            </RoutHasPermission>
                        }/>
                    })}
                </Route> : <Route path="*" element={<Login/>}/>
                }
                <Route path="*" element={<NotFoundPage/>}/>
            </Routes>
        </BrowserRouter>
    );
}

export default App;

import {useFormik} from "formik";
import {useRole} from "../../core/hook";

const CreateOrUpdate = () => {

    const {createRole} = useRole();

    const formik = useFormik({
        initialValues: {
            code: "",
            name: "",
        },
        onSubmit: values => {
            createRole(values)
        },
    });


    return <div data-backdrop="false" className="modal fade" id="createRoleModal" tabIndex="-1" role="dialog"
                aria-labelledby="exampleModalLabel"
                aria-hidden="true">
        <div className="modal-dialog" role="document">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">Create Role</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body">
                    <form onSubmit={formik.handleSubmit}>

                        <div className="form-outline mb-2">
                            <label className="form-label" htmlFor="email">Name</label>
                            <input name="name" type="text" onChange={formik.handleChange}
                                   value={formik.values.name}
                                   className="form-control form-control-lg"/>
                        </div>

                        <div className="form-outline mb-2">
                            <label className="form-label" htmlFor="code">Code</label>
                            <input onChange={formik.handleChange}
                                   value={formik.values.code}
                                   name="code" type="text"
                                   className="form-control form-control-lg"/>

                        </div>

                        <div className="float-end">
                            <button id="close-modal" type="button" className="btn btn-secondary me-3"
                                    data-dismiss="modal">Close
                            </button>
                            <button type="submit" className="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
}

export {CreateOrUpdate}


import axios from "axios";


const reqGetRoles = () => {
    return axios.get("/api/roles");
}


const reqGetRoleById = (id) => {
    return axios.get(`/api/roles/${id}`);
}

const reqCreateRole = (payload) => {
    return axios.post("/api/roles", payload);
}
const reqUpdatePermission = (payload) => {
    return axios.put("/api/roles/permission", payload);
}


export {reqGetRoles, reqCreateRole, reqGetRoleById, reqUpdatePermission}
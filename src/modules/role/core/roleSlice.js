import {createSlice} from "@reduxjs/toolkit";


const roleSlice = createSlice({
    name: "users",
    initialState: {
        list: [],
        loading: false,
        detail: {
            role: null,
            loading: false,
            permissions: [],
        }
    },
    reducers: {
        setRoles: (state, action) => {
            state.list = action.payload;
        },
        setLoading: (state, action) => {
            state.loading = action.payload;
        },
        setDetail: (state, action) => {
            state.detail = {...state.detail, ...action.payload};
        },
        updatePermission: (state, action) => {
            state.detail.permissions = state.detail.permissions.map(permission => {
                if (action.payload.id === permission.id) {
                    permission.status = Number(!!!(permission.status));
                }
                return permission;
            })
        }
    }
})

export const {
    setRoles,
    setLoading,
    setDetail,
    updatePermission
} = roleSlice.actions;

export default roleSlice.reducer;
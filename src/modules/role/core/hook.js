import {reqCreateRole, reqGetRoleById, reqGetRoles, reqUpdatePermission} from "./request";
import {useDispatch} from "react-redux";
import {setDetail, setLoading, setRoles, updatePermission} from "./roleSlice";

const useRole = () => {

    const dispatch = useDispatch();

    const getRoles = () => {
        dispatch(setLoading(true));
        reqGetRoles().then(response => {
            dispatch(setRoles(response.data.data));
            dispatch(setLoading(false));
        });
    }
    const getRoleById = (id) => {
        dispatch(setDetail({loading: true}));
        reqGetRoleById(id).then(response => {
            dispatch(setDetail({...response.data.data, loading: false}));
        });
    }

    const createRole = (payload) => {
        dispatch(setLoading(true));
        return reqCreateRole(payload).then(response => {
            console.log(response)
            dispatch(setLoading(false));
            getRoles();
        }).catch(error => {
            console.log(error)
            dispatch(setLoading(false));
            alert(error.response.data.message);
        })

    }

    const onUpdatePermission = (payload) => {
        dispatch(setDetail({loading: true}));
        reqUpdatePermission(payload).then(() => {
            dispatch(setDetail({loading: false}));
            getRoleById(payload.role_id);
        }).catch(error => {
            dispatch(setDetail({loading: false}));
            alert(error.response.data.message);
        })
    }

    const onTogglePermission = (permission) => dispatch(updatePermission(permission))

    return {getRoles, createRole, getRoleById, onTogglePermission, onUpdatePermission}
}

export {useRole}
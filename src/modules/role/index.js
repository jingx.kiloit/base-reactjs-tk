import React, {useEffect} from 'react'
import {useRole} from "./core/hook";
import {useSelector} from "react-redux";
import {PermissionModal} from "./components/form/PermissionModal";
import {HasPermission} from "../components/HasPermission";
import {CreateOrUpdate} from "./components/form/CreateOrUpdate";

const RolePage = () => {
    const {getRoles, getRoleById} = useRole();
    const {list, loading} = useSelector(state => state.role);

    useEffect(() => {
        getRoles();
        // eslint-disable-next-line
    }, []);

    // console.log(users)

    if (loading) return <div className="d-flex justify-content-center align-items-center w-100 h-100">
        <div className="spinner-border" role="status"/>

    </div>

    return (
        <div>
            <HasPermission permission="create-role">
                <button type="button" data-toggle="modal" data-target="#createRoleModal"
                        className="btn btn-primary float-end mb-3">
                    Add Role
                </button>
            </HasPermission>
            <table className="table border">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {list.map(({name, code, id, createdDate}) => {
                    return (
                        <tr key={id}>
                            <td>{name}</td>
                            <td>{code}</td>
                            <td>{createdDate}</td>
                            <td>
                                <button onClick={() => {
                                    getRoleById(id)
                                }} type="button" data-toggle="modal" data-target="#permissionModal"
                                        className="btn btn-info text-light">
                                    Permissions
                                </button>
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
            <PermissionModal/>
            <CreateOrUpdate/>
        </div>


    )
}


export default RolePage;
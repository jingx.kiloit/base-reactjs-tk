import {reqCreateUsers, reqGetUsers} from "./request";
import {useDispatch} from "react-redux";
import {setList, setLoading} from "./usersSlice";

const useUsers = () => {

    const dispatch = useDispatch();

    const getUsers = () => {
        dispatch(setLoading(true));
        reqGetUsers().then(response => {
            dispatch(setList(response.data.data));
            dispatch(setLoading(false));
        });
    }

    const createUser = (payload) => {
        dispatch(setLoading(true));
        return reqCreateUsers(payload).then(response => {
            console.log(response)
            dispatch(setLoading(false));
            getUsers();
        }).catch(error => {
            console.log(error)
            dispatch(setLoading(false));
            alert(error.response.data.message);
        })

    }

    return {getUsers, createUser}
}

export {useUsers}
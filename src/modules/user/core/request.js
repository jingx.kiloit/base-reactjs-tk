import axios from "axios";


const reqGetUsers = () => {
    return axios.get("/api/user");
}

const reqCreateUsers = (payload) => {
    return axios.post("/api/user", payload);
}


export {reqGetUsers, reqCreateUsers}
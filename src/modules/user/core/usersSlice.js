import {createSlice} from "@reduxjs/toolkit";


const usersSlice = createSlice({
    name: "users",
    initialState: {
        list: [],
        loading: false
    },
    reducers: {
        setList: (state, action) => {
            state.list = action.payload;
        },
        setLoading: (state, action) => {
            state.loading = action.payload;
        }
    }
})

export const {
    setList,
    setLoading
} = usersSlice.actions;

export default usersSlice.reducer;
import React, {useEffect} from 'react'
import {useUsers} from "./core/hook";
import {useSelector} from "react-redux";
import {CreateOrUpdate} from "./components/form/CreateOrUpdate";
import {HasPermission} from "../components/HasPermission";
import {useRole} from "../role/core/hook";

const UserPage = () => {

    const {getUsers} = useUsers();
    const {getRoles} = useRole();

    const {list, loading} = useSelector(state => state.users);

    useEffect(() => {
        getUsers();
        getRoles();
        // eslint-disable-next-line
    }, []);


    if (loading) return <div className="d-flex justify-content-center align-items-center w-100 h-100">
        <div className="spinner-border" role="status"/>

    </div>

    return (
        <div>
            <HasPermission permission="create-user">
                <button type="button" data-toggle="modal" data-target="#exampleModal"
                        className="btn btn-primary float-end mb-3">
                    Add User
                </button>
            </HasPermission>
            <table className="table border">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                {list.map(user => {
                    return (
                        <tr key={user.id}>
                            <td>{user.name}</td>
                            <td>{user.username}</td>
                            <td>{user.email}</td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
            <CreateOrUpdate/>
        </div>


    )
}


export default UserPage;
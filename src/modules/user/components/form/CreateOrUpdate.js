import {useFormik} from "formik";
import {useUsers} from "../../core/hook";
import {useSelector} from "react-redux";

const CreateOrUpdate = () => {
    const {list} = useSelector(state => state.role);
    const {createUser} = useUsers();

    const formik = useFormik({
        initialValues: {
            name: "",
            username: "",
            gender: "Male",
            email: "",
            phone: "",
            password: "",
            confirm_password: "",
            // salary: 1200,
            hire_date: "",
            role_id: ""
        },
        onSubmit: values => {
            console.log(values)
            createUser(values)
        },
    });


    return <div data-backdrop="false" className="modal fade" id="exampleModal" tabIndex="-1" role="dialog"
                aria-labelledby="exampleModalLabel"
                aria-hidden="true">
        <div className="modal-dialog" role="document">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">Create User</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body">
                    <form onSubmit={formik.handleSubmit}>

                        <div className="form-outline mb-2">
                            <label className="form-label" htmlFor="email">Name</label>
                            <input name="name" type="text" onChange={formik.handleChange}
                                   value={formik.values.name}
                                   className="form-control form-control-lg"/>
                        </div>

                        <div className="form-outline mb-2">
                            <label className="form-label" htmlFor="username">Username</label>
                            <input onChange={formik.handleChange}
                                   value={formik.values.username}
                                   name="username" type="text"
                                   className="form-control form-control-lg"/>

                        </div>

                        <div className="form-outline mb-2">
                            <label className="form-label" htmlFor="role">Select Gender</label>
                            <select name="gender"
                                    value={formik.values.gender}
                                    onChange={formik.handleChange}
                                    className="form-control">
                                <option value="Male" label="Male"/>
                                <option value="Female" label="Female"/>
                                <option value="Other" label="Other"/>
                            </select>
                        </div>

                        <div className="form-outline mb-2">
                            <label className="form-label" htmlFor="role_id">Select Role</label>
                            <select name="role_id"
                                    value={formik.values.role_id}
                                    onChange={formik.handleChange}
                                    className="form-control">
                                {list.map(({name, id}) => {
                                    return <option key={id} value={id} label={name}/>
                                })}
                            </select>
                        </div>

                        <div className="form-outline mb-2">
                            <label className="form-label" htmlFor="phone">Phone</label>
                            <input name="phone" type="phone" onChange={formik.handleChange}
                                   value={formik.values.phone}
                                   className="form-control form-control-lg"/>

                        </div>
                        <div className="form-outline mb-2">
                            <label className="form-label" htmlFor="email">Email</label>
                            <input name="email" type="email" onChange={formik.handleChange}
                                   value={formik.values.email}
                                   className="form-control form-control-lg"/>

                        </div>


                        <div className="form-outline mb-2">
                            <label className="form-label" htmlFor="password">Password</label>
                            <input name="password" type="password"
                                   onChange={formik.handleChange}
                                   value={formik.values.password}
                                   className="form-control form-control-lg"/>
                        </div>
                        <div className="form-outline mb-2">
                            <label className="form-label" htmlFor="confirm_password">Confirm Password</label>
                            <input name="confirm_password" type="password"
                                   onChange={formik.handleChange}
                                   value={formik.values.confirm_password}
                                   className="form-control form-control-lg"/>
                        </div>

                        <div className="form-outline mb-2">
                            <label className="form-label" htmlFor="hire_date">Hire Date</label>
                            <input name="hire_date" type="date"
                                   onChange={formik.handleChange}
                                   value={formik.values.hire_date}
                                   className="form-control form-control-lg"/>
                        </div>

                        <div className="float-end">
                            <button id="close-modal" type="button" className="btn btn-secondary me-3"
                                    data-dismiss="modal">Close
                            </button>
                            <button type="submit" className="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
}

export {CreateOrUpdate}


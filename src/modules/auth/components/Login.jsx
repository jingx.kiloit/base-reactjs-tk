import {useState} from "react";
import {useAuth} from "../core/hook";

const Login = () => {
    const [payload, setPayload] = useState({
        username: "admin",
        password: "123"
    });

    const {onLogin} = useAuth();

    const handleChange = (e) => {
        setPayload({
            ...payload,
            [e.target.name]: e.target.value
        });
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        onLogin(payload);
    }


    return <section className="vh-100">
        <div className="container py-5 h-100">
            <div className="row d-flex align-items-center justify-content-center h-100">
                <div className="col-md-8 col-lg-7 col-xl-6">
                    <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.svg"
                         className="img-fluid" alt="__"/>
                </div>
                <div className="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
                    <form onSubmit={handleSubmit}>
                        <div className="form-outline mb-4">
                            <input value={payload.username} onChange={handleChange} name="username" type="text"
                                   id="form1Example13"
                                   className="form-control form-control-lg"/>
                            <label className="form-label" htmlFor="form1Example13">Username</label>
                        </div>

                        <div className="form-outline mb-4">
                            <input value={payload.password} onChange={handleChange} name="password" type="password"
                                   id="form1Example23"
                                   className="form-control form-control-lg"/>
                            <label className="form-label" htmlFor="form1Example23">Password</label>
                        </div>

                        <button type="submit" className="btn btn-primary btn-lg btn-block">Sign in</button>
                    </form>
                </div>
            </div>
        </div>
    </section>;
}

export default Login;
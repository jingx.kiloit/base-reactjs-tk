import axios from "axios";


const reqLogin = (payload = {}) => {
    return axios.post("/api/auth/login", payload, {
        headers: {
            Authorization: "Basic ZzM6MTIz"
        }
    })
}
const reqGetProfile = (payload = {}) => {
    return axios.get("/api/user/profile", payload);
}


export {reqLogin, reqGetProfile}
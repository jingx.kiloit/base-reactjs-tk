import UserPage from "../user";
import RolePage from "../role";


const routs = [
    {
        path: "/",
        exact: true,
        component: UserPage,
        permission: "list-user"
    },
    {
        path: "/role",
        exact: false,
        component: RolePage,
        permission: "list-role"
    }
]

export {routs}
const menus = [
    {
        path: "/",
        exact: true,
        title: "User",
        permission: "list-user"
    },
    {
        path: "/role",
        exact: false,
        title: "Role",
        permission: "list-role"
    },
    {
        path: "/food",
        exact: false,
        title: "Food",
        permission: "list-food"
    }
]

export {menus}
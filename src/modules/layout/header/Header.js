import React from 'react'
import {Link, useLocation} from "react-router-dom"
import {menus} from "../../constants/menus";
import {HasPermission} from "../../components/HasPermission";


export const Header = () => {

    const location = useLocation();


    const classActive = (path) => {
        if (location.pathname === path)
            return "text-primary text-decoration-underline";
        return "";
    }
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light px-4">
            <Link className="navbar-brand" to="/">Demo</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    {
                        menus.map((menu, index) => {
                            const {path, title, permission} = menu;
                            return <HasPermission key={index} permission={permission}>
                                <li className="nav-item">
                                    <Link className={`nav-link ${classActive(path)}`} to={path}>{title}</Link>
                                </li>
                            </HasPermission>
                        })
                    }


                </ul>

            </div>
            <button className="btn btn-danger float-end" onClick={() => {
                localStorage.removeItem("token");
                window.location.reload();
            }}>
                Logout
            </button>
        </nav>
    )
}

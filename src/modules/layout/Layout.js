import { Outlet } from "react-router-dom"

import { Header } from "./header/Header";
import React from "react";


const Layout = () => {
    return <>
        <Header />
        <div className="p-5">
            <Outlet />
        </div>
    </>
}

export default Layout;
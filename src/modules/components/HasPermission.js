import {Navigate} from "react-router-dom";
import {getPermissions} from "../helper/axiosHelper";


const isAllowed = (permission) => {
    const usersPermissions = getPermissions();
    return usersPermissions.includes(permission);
}

const HasPermission = ({children, permission}) => {
    if (isAllowed(permission)) {
        return children;
    }
    return null;
}

const RoutHasPermission = ({children, permission}) => {

    if (isAllowed(permission)) {
        return children;
    }
    return <Navigate to="/404"/>;
}

export {HasPermission, RoutHasPermission}
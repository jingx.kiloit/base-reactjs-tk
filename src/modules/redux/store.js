import {configureStore} from '@reduxjs/toolkit'
import authSlice from "../auth/core/authSlice";
import usersSlice from "../user/core/usersSlice";
import roleSlice from "../role/core/roleSlice";


export const store = configureStore({
    reducer: {
        auth: authSlice,
        users: usersSlice,
        role: roleSlice
    }
})